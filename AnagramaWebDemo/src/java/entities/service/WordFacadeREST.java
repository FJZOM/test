/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import entities.Word;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import resources.Dicctionary;

/**
 *
 * @author felipe.oviedo.maltez
 */
@Stateless
@Path("entities.word")
public class WordFacadeREST extends AbstractFacade<Word> {

    @PersistenceContext(unitName = "AnagramaWebDemoPU")
    private EntityManager em;

    public WordFacadeREST() {
        super(Word.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Word entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Word entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Word find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    public List<Word> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Word> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @GET
    @Path("findWords/{description}")
    @Produces(MediaType.TEXT_PLAIN)
    public String findWords(@PathParam("description") String description) throws Exception {
        
        String strmatch = new String();
        Dicctionary dic = new Dicctionary();
        
        ArrayList<String> arrStr = dic.getDicctionary("http://www.math.sjsu.edu/~foster/dictionary.txt");

        
        for(int i= 1; i < arrStr.size(); i++){
        char[] word1 = description.replaceAll("[\\s]", "").toCharArray();
        char[] word2 = arrStr.get(i).replaceAll("[\\s]", "").toCharArray();
        Arrays.sort(word1);
        Arrays.sort(word2);
        
       if(Arrays.equals(word1, word2)){
       strmatch = arrStr.get(i);       
       return strmatch;
       }
       else
           strmatch = "NO MATCH";
       
        }
       
       return strmatch;
           

    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
