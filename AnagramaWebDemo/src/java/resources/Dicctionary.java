/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.net.URL;
import java.net.URLConnection;
import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author felipe.oviedo.maltez
 */
public class Dicctionary {
//Get info.Feature 1
//Dictionary info. featutre 2
    public   ArrayList<String> getDicctionary(String url) throws Exception {
        URL website = new URL(url);
        ArrayList<String> dictionaryList = new ArrayList<>();
        URLConnection connection = website.openConnection();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine); 
            dictionaryList.add(inputLine);
        }
        in.close();
 
        return dictionaryList;
    }
 

}
